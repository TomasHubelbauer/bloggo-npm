# NPM

- [ ] Compare with [PNPM](https://github.com/pnpm/pnpm) which is written in TypeScript and looks real nice

## NPM Tips

- `npm ci` skips resolving exact versions from `package.json` and uses directly the lockfile versions for 100 % reproducible builds

## My NPM packages

### [`net-tree`](https://www.npmjs.com/package/net-tree)

Accepts items with `superIds` and `priorIds` and returns an array of sorted items with computed `level`s.

### [`qr-channel`](https://www.npmjs.com/package/qr-channel)

Provides infrastructure for using webcams to exchange data between devices by displaying and scanning QR codes.

### [`markdown-dom`](https://www.npmjs.com/package/markdown-dom)

Parses MarkDown files into a structure.

### [`qrcode-generator-render-to-2d-context`](https://www.npmjs.com/package/qrcode-generator-render-to-2d-context)

**Deprecated:** The fork has now been mainstreamed.

A [fork](https://github.com/TomasHubelbauer/qrcode-generator) of [`qrcode-generator`](https://github.com/kazuhikoarase/qrcode-generator).

Includes [a PR](https://github.com/kazuhikoarase/qrcode-generator/pull/50) for adding a `renderTo2dContext` method.
